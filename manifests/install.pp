
class wekaioclient::install inherits wekaioclient {

    # /opt/weka is always mounted, so set in ibex::

    $cmd = "downloaded wekaio from ${bootstrap_host}"

    exec { $cmd :
        command =>  "/usr/bin/curl http://${bootstrap_host}:${bootstrap_port}/dist/v1/install | /usr/bin/sh",
        unless  =>  "/usr/bin/which weka"
    }

    # no longer necessary to start the agent (at least from 3.13.6)

    file { '/etc/systemd/system/weka-agent.service':
        source  =>  'puppet:///modules/wekaioclient/weka-agent.service',
        owner   =>  'root',
        group   =>  'root',
        mode    =>  '0755'
    } ~> 
    exec { 'WekaIOClient: systemd reload':
        command     =>  '/usr/bin/systemctl daemon-reload',
        refreshonly =>  true
    }

}

