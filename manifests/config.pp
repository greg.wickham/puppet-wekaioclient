
class wekaioclient::config inherits wekaioclient {

    file { '/etc/wekaio':
        ensure  =>  'directory',
        owner   =>  'root',
        group   =>  'root',
        mode    =>  '0755'
    }

    create_resources( 'file_line', $config_lines, {
        path    =>  '/etc/wekaio/service.conf'
    } )

    create_resources( 'ini_setting', $config_ini, {
        ensure  =>  'present',
        path    =>  '/etc/wekaio/service.conf'
    } )


}

