
class wekaioclient::mount inherits wekaioclient {

    $directories.each | $name | {
        file { $name :
            ensure  =>  'directory'
        }   
    }

    if ( $net ) {

        $use_net = $net

    } else {

        $ib_ifaces = keys( fact('networking.interfaces') ).filter | String $ifname | {
            $ifname =~ /^i/
        }
    
        if ( $ib_ifaces.size == 0 ) {
            fail("No IB interfaces available")
        }

        $use_net = $ib_ifaces[0]

    }

    $mode = get( $enc, 'wekaio.dedicated', $dedicated ) ? {
        true    =>  [],
        default =>  [ 'dedicated_mode=none' ]
    }

    $mounts.each | String $name, String $device | {

        file { $name :
            ensure  =>  'directory'
        }

        $options = [ $fs_cores.map | $x | { "core=${x}" } ] + $mode + $mount_options + 
            [ "net=${use_net}" ]

        if ( $verbose ) {
            $tmp1 = $options.flatten.join(',')
            $tmp2 = sprintf('%s/%s', $mount_hosts.join(','), $device )
            notify{ "mounting ${name} with options '${tmp1}' for device ${tmp2}": }
        }

        if ( $systemd ) {

            $mount_name = inline_template('<%= @name.gsub(/\//,"-").sub(/^-*/,"").sub(/-*$/,"") -%>')

            # ensure the systemd mount point file exists
            file { "/etc/systemd/system/${mount_name}.mount":
                content =>  template('wekaioclient/systemd-unit.mount.erb'),
                owner   =>  'root',
                group   =>  'root',
                mode    =>  '0644'
            }

            # test if the fs is mounted by inspecting facts
            if ( ! ( $name in fact('mountpoints') ) ) {

                # fs is not mounted, so mount
                exec { "mounting ${name}":
                    command     =>  "/usr/bin/systemctl enable ${mount_name}.mount --now"
                }

            } else {

                # $name is mounted, but to be sure compare the fs type

                $mounted_fs = fact("mountpoints.${name}.filesystem")

                if ( $mounted_fs != 'wekafs' ) {
                    # fs type doesn't match, so throw an error
                    fail("mount ${name} already mounted as type ${mounted_fs}")
                }

            }

        } else {

            mount { $name :
                fstype      =>  'wekafs',
                ensure      =>  'mounted',
                remounts    =>  $remount,
                options     =>  $options.flatten.join(','),
                device      =>  sprintf('%s/%s', $mount_hosts.join(','), $device )
            }


        }

    }

    $links.each | $src, $dst | {
        file { $src :
            target  =>  $dst
        }
    }

}

