
#
# if 'wekaio.cores' is defined in $enc then use that, otherwise
# use the heira value.
#

class wekaioclient(
    String              $bootstrap_host,
    Integer             $bootstrap_port,
    Array[String]       $mount_hosts,
    Hash                $config_lines   = $::wekaioclient::params::config_lines,
    Hash                $config_ini     = $::wekaioclient::params::config_ini,
    Boolean             $remount        = $::wekaioclient::params::remount,
    Hash[String,String] $mounts         = $::wekaioclient::params::mounts,
    Array[String]       $directories    = $::wekaioclient::params::directories,
    Hash[String,String] $links          = $::wekaioclient::params::links,
    Array[String]       $mount_options  = $::wekaioclient::params::mount_options,
    Array[Integer]      $cores          = $::wekaioclient::params::cores,
    Boolean             $dedicated      = $::wekaioclient::params::dedicated,
    Optional[String]    $net            = $::wekaioclient::params::net,
    Boolean             $systemd        = $::wekaioclient::params::systemd,
    Boolean             $verbose        = $::wekaioclient::params::verbose
) inherits wekaioclient::params {

    $fs_cores = get( $enc, 'wekaio.cores', $cores )

    if ( ! $fs_cores ) {
        fail("CMDB wekaio.cores is not defined - please define to enable WekaIO")
    }

    include wekaioclient::install
    include wekaioclient::config
    include wekaioclient::mount

    Class['wekaioclient::install'] ->
    Class['wekaioclient::config'] ->
    Class['wekaioclient::mount']

}

