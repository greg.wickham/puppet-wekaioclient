
class wekaioclient::params {

    $mounts = {}
    $directories = []
    $mount_mode = 'dedicated'
    $mount_options = []
    $links = {}
    $cores = []
    $dedicated = true
    $net = undef
    $systemd = false
    $verbose = false
    $remount = false
    $config_lines = {}

}

